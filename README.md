## Pluggable Ruby Modules

## Generating Ruby Code

Generate protobuf generated file for module-1 in module-2's context:

```
protoc --ruby_out=modules/my_module_2 proto/my_module_1.proto
```
